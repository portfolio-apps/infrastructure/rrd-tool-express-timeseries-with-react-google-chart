const express = require('express')
const { exec } = require("child_process");
const cors = require('cors');
const app = express()
const port = 5000

const corsOption = {
    origin: ['http://localhost:3001'],
};
app.use(cors(corsOption));

function fetchSeries(stdout) {
  const series = stdout.split("\n");
  const newSeries = []
  series.forEach(obj => {
    const entry = obj.split(": ")
    if (entry[0] && entry[1]) {
      newSeries.push({
        timestamp: parseInt(entry[0]),
        value: parseFloat(entry[1])
      })
    }
  })
  return newSeries
}

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get("/series/:start/:stop", async (req, res) => {
    const timestamp = new Date().getTime();
    const now = parseInt(timestamp / 1000) - 2
    // const now = parseInt(timestamp) - 2

    exec(`bash ../scripts/fetchAvg.sh ${req.params.start} ${req.params.stop}`,

    (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        
        const newSeries = fetchSeries(stdout)

        res.status(200).json({ data: newSeries });
    });
});


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})