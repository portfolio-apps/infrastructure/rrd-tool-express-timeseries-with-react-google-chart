# RRD Tool Express Timeseries with React Google Chart

You'll most likely need to install RRD Tool on your os. We used Ubuntu 20.04 WSL2.

### Install RRD Tool on Ubuntu 20.04
<pre>
sudo apt-get install rrdtool
</pre>

### Create DB
<pre>
    bash scripts/create.sh  
</pre>

### Start a random number GEN between 0-100 date loop
<pre>
    bash scripts/insert.sh
</pre>

### Start API Express Server in new terminal from project root
<pre>
    cd server 
    npm i && npm start

    <b>Test routes in server/api.http</b>
</pre>

### Start Frontend with React and Google Charts in new terminal from project root
<pre>
    cd client 
    npm i && npm start
</pre>